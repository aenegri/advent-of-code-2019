package solutions

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

// RunDay2 solves day 2.
func RunDay2() {
	file, err := os.Open("input/day2part1.txt")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	scanner.Scan() // only one line
	inputs := strings.Split(scanner.Text(), ",")
	nums := []int{}

	for _, input := range inputs {
		num, err := strconv.Atoi(input)

		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, num)
	}

	nums[1] = 12
	nums[2] = 2

	runIntcodeProgram(nums)
	fmt.Println("Part 1 - Restory Gravity Assist")
	fmt.Printf("===> Output: %v", runIntcodeProgram(nums)[0])
	fmt.Println("")
	fmt.Println("Part 2 - Determining Inputs")
	fmt.Printf("===> Output: %d", findNounVerb(nums, 19690720))
}

func runIntcodeProgram(nums []int) []int {
	program := make([]int, len(nums))
	copy(program, nums)

	for index := 0; index < len(program); index += 4 {
		op := program[index]
		switch op {
		case 1:
			program[program[index+3]] = program[program[index+1]] + program[program[index+2]]
		case 2:
			program[program[index+3]] = program[program[index+1]] * program[program[index+2]]
		case 99:
			fallthrough
		default:
			break
		}
	}

	return program
}

func findNounVerb(nums []int, target int) int {
	program := make([]int, len(nums))

	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			copy(program, nums)

			program[1] = noun
			program[2] = verb

			program = runIntcodeProgram(program)

			if program[0] == target {
				return 100*noun + verb
			}
		}
	}

	return -1
}
