package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

// RunDay6 solves for day 6.
func RunDay6() {
	data, err := ioutil.ReadFile("./input/day6part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(data), "\n")

	orbitMap := parseOrbitMap(lines)

	fmt.Println("Part 1 - Orbit Count")
	fmt.Printf("===> Output: %d\n", countOrbits(orbitMap))
	fmt.Println("Part 2 - Distance from YOU to SAN")
	fmt.Printf("===> Output: %d", distanceToSanta(orbitMap))
}

type spaceObject struct {
	name     string
	children []*spaceObject
	parents  []*spaceObject
}

func parseOrbitMap(lines []string) map[string]*spaceObject {
	orbitMap := map[string]*spaceObject{}

	for _, line := range lines {
		parts := strings.Split(line, ")")

		innerObject, ok := orbitMap[parts[0]]

		if !ok {
			innerObject = &spaceObject{
				name:     parts[0],
				children: []*spaceObject{},
				parents:  []*spaceObject{},
			}
			orbitMap[parts[0]] = innerObject
		}

		outerObject, ok := orbitMap[parts[1]]

		if !ok {
			outerObject = &spaceObject{
				name:     parts[1],
				children: []*spaceObject{},
				parents:  []*spaceObject{},
			}
			orbitMap[parts[1]] = outerObject
		}

		innerObject.children = append(innerObject.children, outerObject)
		outerObject.parents = append(outerObject.parents, innerObject)
	}

	return orbitMap
}

func countOrbits(objects map[string]*spaceObject) int {
	total, depth := 0, 1
	queue := []*spaceObject{objects["COM"]}
	nextLevel := []*spaceObject{}
	for len(queue) > 0 {

		for len(queue) > 0 {
			object := queue[0]
			queue = queue[1:]

			for _, child := range object.children {
				nextLevel = append(nextLevel, child)
				total += depth
			}
		}

		queue = nextLevel
		nextLevel = []*spaceObject{}
		depth++
	}

	return total
}

func distanceToSanta(orbitMap map[string]*spaceObject) int {
	dist := 0
	queue := []*spaceObject{orbitMap["YOU"]}
	nextLevel := []*spaceObject{}
	visited := map[string]bool{}

	for len(queue) > 0 {

		for len(queue) > 0 {
			// pop from queue
			object := queue[0]
			queue = queue[1:]
			// check if at santa
			if object.name == "SAN" {
				return dist - 2
			}
			// add children and parents to queue(but not self)
			for _, node := range object.children {
				if !visited[node.name] {
					nextLevel = append(nextLevel, node)
					visited[node.name] = true
				}
			}
			for _, node := range object.parents {
				if !visited[node.name] {
					nextLevel = append(nextLevel, node)
					visited[node.name] = true
				}
			}
		}

		queue = nextLevel
		nextLevel = []*spaceObject{}
		dist++
	}

	return -1
}
