package solutions

import (
	"fmt"
	"gitlab.com/aenegri/advent-of-code-2019/ampcircuit"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// RunDay7 solves for day 7.
func RunDay7() {
	data, err := ioutil.ReadFile("./input/day7part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	tokens := strings.Split(string(data), ",")

	nums := []int{}
	for _, token := range tokens {
		num, err := strconv.Atoi(token)

		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, num)
	}

	circuit := ampcircuit.NewCircuit(nums, []int{0, 1, 2, 3, 4}, false)
	feedbackCircuit := ampcircuit.NewCircuit(nums, []int{5, 6, 7, 8, 9}, true)

	fmt.Println("Part 1 - Maximum Thrust")
	fmt.Printf("===> Output: %d\n", circuit.GetMaxOutput())
	fmt.Println("Part 2 - Thrusting With Feedback")
	fmt.Printf("===> Output: %d\n", feedbackCircuit.GetMaxOutput())

	//circuit
}
