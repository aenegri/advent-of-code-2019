package solutions

import (
	"fmt"
	"gitlab.com/aenegri/advent-of-code-2019/intcode"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// RunDay5 solves for day 5.
func RunDay5() {
	data, err := ioutil.ReadFile("./input/day5part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	tokens := strings.Split(string(data), ",")

	nums := []int{}
	for _, token := range tokens {
		num, err := strconv.Atoi(token)

		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, num)
	}

	intcode := intcode.NewIntcode(intcode.Options{
		Inputs:  []int{1},
		Program: nums,
	})

	intcode.Run()
	output1 := intcode.Output
	intcode.Reset()
	intcode.Inputs = []int{5}
	intcode.Run()
	output2 := intcode.Output

	fmt.Println("Part 1 - Diagnosing the Intcode Program")
	fmt.Printf("===> Output: %v\n", output1)
	fmt.Println("Part 2 - Even More Ops")
	fmt.Printf("===> Output: %v\n", output2)
}
