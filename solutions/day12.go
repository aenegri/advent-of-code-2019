package solutions

import "fmt"

// RunDay12 solves for day 12.
func RunDay12() {
	sim := newGravitySim([]*moon{
		newMoon([]int{13, -13, -2}, []int{0, 0, 0}),
		newMoon([]int{16, 2, -15}, []int{0, 0, 0}),
		newMoon([]int{7, -18, -12}, []int{0, 0, 0}),
		newMoon([]int{-3, -8, -8}, []int{0, 0, 0}),
	})
	sim2 := newGravitySim([]*moon{
		newMoon([]int{13, -13, -2}, []int{0, 0, 0}),
		newMoon([]int{16, 2, -15}, []int{0, 0, 0}),
		newMoon([]int{7, -18, -12}, []int{0, 0, 0}),
		newMoon([]int{-3, -8, -8}, []int{0, 0, 0}),
	})
	sim.Simuate(1000)
	fmt.Println("Part 1 - Total Energy in System")
	fmt.Printf("===> Output: %d\n", sim.CurrentTotalEnergy())
	sim2.CycleLength()
}

type gravitySim struct {
	initialState []*moon
	moons        []*moon
}

type moon struct {
	pos []int
	vel []int
}

func newGravitySim(moons []*moon) *gravitySim {
	initialMoons := make([]*moon, len(moons))
	for i, moon := range moons {
		pos := make([]int, len(moon.pos))
		vel := make([]int, len(moon.vel))
		copy(pos, moon.pos)
		copy(vel, moon.vel)
		initialMoons[i] = newMoon(pos, vel)
	}
	return &gravitySim{
		initialState: initialMoons,
		moons:        moons,
	}
}

func (sim *gravitySim) Simuate(steps int) {
	for step := 0; step < steps; step++ {
		for i := 0; i < len(sim.moons); i++ {
			moonA := sim.moons[i]
			for j := i + 1; j < len(sim.moons); j++ {
				moonB := sim.moons[j]
				moonA.ApplyGravity(moonB)
			}
			moonA.ApplyVelocity()
		}
	}
}

func (sim *gravitySim) CurrentTotalEnergy() int {
	total := 0
	for _, moon := range sim.moons {
		total += moon.GetEnergy()
	}
	return total
}

func (sim *gravitySim) CycleLength() int {
	sim.Simuate(1)
	dimensionCycleLength := make([]int, 3)
	for dimension := 0; dimension < len(dimensionCycleLength); dimension++ {
		steps := 1
		for !sim.IsAtOriginalState(dimension) {
			for i := 0; i < len(sim.moons); i++ {
				moonA := sim.moons[i]
				for j := i + 1; j < len(sim.moons); j++ {
					moonB := sim.moons[j]
					moonA.ApplyGravityDimension(moonB, dimension)
				}
				moonA.ApplyVelocityDimension(moonA.vel[dimension], dimension)
			}
			steps++
		}
		// note - used calculator to get lowest common multiple
		// consider implementing later on
		fmt.Println(steps)
		dimensionCycleLength[dimension] = steps
	}
	return 0
}

func (sim *gravitySim) IsAtOriginalState(dimension int) bool {
	for i := 0; i < len(sim.moons); i++ {
		originalMoon := sim.initialState[i]
		currentMoon := sim.moons[i]

		if originalMoon.pos[dimension] != currentMoon.pos[dimension] ||
			originalMoon.vel[dimension] != currentMoon.vel[dimension] {
			return false
		}
	}
	return true
}

func newMoon(pos []int, vel []int) *moon {
	return &moon{pos: pos, vel: vel}
}

// ApplyGravity adjusts velocity based on the relative location
// of the moon parameter.
func (m *moon) ApplyGravity(other *moon) {
	for index := range m.pos {
		m.ApplyGravityDimension(other, index)
	}
}

// ApplyVelocity changes position based on current velocity.
func (m *moon) ApplyVelocity() {
	for index, change := range m.vel {
		m.ApplyVelocityDimension(change, index)
	}
}

func (m *moon) ApplyGravityDimension(other *moon, dimension int) {
	if m.pos[dimension] < other.pos[dimension] {
		m.vel[dimension]++
		other.vel[dimension]--
	} else if m.pos[dimension] > other.pos[dimension] {
		m.vel[dimension]--
		other.vel[dimension]++
	}
}

func (m *moon) ApplyVelocityDimension(velocity int, dimension int) {
	m.pos[dimension] += velocity
}

func (m *moon) GetEnergy() int {
	potential, kinetic := 0, 0

	for _, value := range m.pos {
		if value < 0 {
			value = -value
		}
		potential += value
	}

	for _, value := range m.vel {
		if value < 0 {
			value = -value
		}
		kinetic += value
	}
	return potential * kinetic
}
