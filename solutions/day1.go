package solutions

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

// RunDay1 solves day 1.
func RunDay1() {
	file, err := os.Open("input/day1part1.txt")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	masses := []int{}
	for scanner.Scan() {
		mass, err := strconv.Atoi(scanner.Text())

		if err != nil {
			log.Fatal(err)
		}
		masses = append(masses, mass)
	}

	fuel := getFuelRequirement(masses)

	fmt.Println("Part 1 - Calculate Fuel")
	fmt.Printf("==> Output - %d", fuel)
	fmt.Println()

	fuel = getFuelRequirementAgain(masses)
	fmt.Println("Part 2 - Calculate Fuel for Fuel")
	fmt.Printf("==> Output: %d", fuel)
	fmt.Println()
}

func getFuelRequirement(mass []int) int {
	totalMass := 0
	for _, mass := range mass {
		totalMass += (mass / 3) - 2
	}
	return totalMass
}

func getFuelRequirementAgain(masses []int) int {
	totalMass := 0
	memo := map[int]int{}
	for _, mass := range masses {
		fuel := (mass / 3) - 2
		extraFuel := getFuelForFuel(fuel, memo)
		totalMass += fuel + extraFuel
	}
	return totalMass
}

func getFuelForFuel(mass int, memo map[int]int) int {
	if val, ok := memo[mass]; ok {
		return val
	} else if mass < 9 {
		return 0
	} else {
		moreFuel := (mass / 3) - 2
		total := moreFuel + getFuelForFuel(moreFuel, memo)
		memo[mass] = total
		return total
	}
}
