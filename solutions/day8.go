package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// RunDay8 solves for day 8.
func RunDay8() {
	data, err := ioutil.ReadFile("./input/day8part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	img := newEncodedImage(string(data), 6, 25)

	fmt.Println("Part 1 - Validate Encoded Image")
	fmt.Printf("===> Output: %d\n", img.Validate())
	fmt.Println("Part 2 - Decode Image")
	img.PrintDecoded()
}

// Pixel encodings
const (
	PixelBlack       = 0
	PixelWhite       = 1
	PixelTransparent = 2
)

type encodedImage struct {
	Height int
	layers []map[coordinate]int
	Width  int
}

func newEncodedImage(input string, height int, width int) *encodedImage {
	parts := strings.Split(input, "")

	pixelsPerLayer := height * width
	layers := make([]map[coordinate]int, len(parts)/pixelsPerLayer)

	for i, part := range parts {
		x := i % width
		y := (i / width) % height
		layerIndex := i / (pixelsPerLayer)

		if layers[layerIndex] == nil {
			layers[layerIndex] = map[coordinate]int{}
		}
		layer := layers[layerIndex]

		pixel, err := strconv.Atoi(part)

		if err != nil {
			log.Fatal(err)
		}

		layer[coordinate{X: x, Y: y}] = pixel
	}

	return &encodedImage{
		Height: height,
		layers: layers,
		Width:  width,
	}
}

// Validate checks that the image is not corrupted.
func (img *encodedImage) Validate() int {
	minZero := img.Height * img.Width
	minZeroLayer := 0
	for i, layer := range img.layers {
		zeroCount := 0

		for _, pixel := range layer {
			if pixel == 0 {
				zeroCount++
			}
		}

		if zeroCount < minZero {
			minZero = zeroCount
			minZeroLayer = i
		}
	}

	layer := img.layers[minZeroLayer]

	oneCount, twoCount := 0, 0
	for _, pixel := range layer {
		switch pixel {
		case 1:
			oneCount++
		case 2:
			twoCount++
		}
	}

	return oneCount * twoCount
}

// Decode compiles the layers into a single image.
func (img *encodedImage) PrintDecoded() {
	unfilledCoordinates := map[coordinate]bool{}

	for x := 0; x < img.Width; x++ {
		for y := 0; y < img.Height; y++ {
			unfilledCoordinates[coordinate{X: x, Y: y}] = true
		}
	}

	decodedImg := map[coordinate]int{}

	for _, layer := range img.layers {
		for coordinate := range unfilledCoordinates {
			if layer[coordinate] != PixelTransparent {
				decodedImg[coordinate] = layer[coordinate]
				delete(unfilledCoordinates, coordinate)
			}
		}
	}

	for y := 0; y < img.Height; y++ {
		for x := 0; x < img.Width; x++ {
			fmt.Printf("%d ", decodedImg[coordinate{X: x, Y: y}])
		}
		fmt.Println()
	}
}
