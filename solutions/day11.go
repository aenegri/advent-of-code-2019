package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"gitlab.com/aenegri/advent-of-code-2019/intcode"
)

// RunDay11 solves for day 11.
func RunDay11() {
	data, err := ioutil.ReadFile("./input/day11part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	tokens := strings.Split(string(data), ",")

	nums := []int{}
	for _, token := range tokens {
		num, err := strconv.Atoi(token)

		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, num)
	}

	robot := newPaintRobot(nums)
	robot.PaintHull(black)
	fmt.Println("Part 1 - Unique Panels Painted")
	fmt.Printf("===> Output: %d\n", robot.UniquePaintCount())

	robot.Reset()
	robot.PaintHull(white)
	fmt.Println("Part 2 - Get Registration")
	fmt.Println("===> Output:")
	robot.Print()
}

const (
	turnLeft  = 0
	turnRight = 1

	black = 0
	white = 1
)

type paintRobot struct {
	boundBottom   int
	boundLeft     int
	boundRight    int
	boundTop      int
	cpu           *intcode.Intcode
	paintedPanels map[coordinate]bool
	panels        map[coordinate]int
}

func newPaintRobot(program []int) *paintRobot {
	options := intcode.Options{
		Program: program,
	}
	return &paintRobot{
		cpu:           intcode.NewIntcode(options),
		paintedPanels: map[coordinate]bool{},
		panels:        map[coordinate]int{},
	}
}

func (r *paintRobot) UniquePaintCount() int {
	return len(r.paintedPanels)
}

func (r *paintRobot) PaintHull(startColor int) {
	// run through intcode program
	// first, set input to color of current paint
	// on first output, paint panel below robot
	// on second output, move left or right one panel
	// movement based on currently facing direction
	dir := up
	x, y, outputCount := 0, 0, 0
	r.cpu.Inputs = []int{startColor}
	r.cpu.OnOutput = func(out int) {
		if outputCount%2 == 0 {
			// paint current spot
			coord := coordinate{X: x, Y: y}
			r.paintedPanels[coord] = true
			r.panels[coord] = out
		} else {
			// move left or right by one panel
			if dir == up && out == turnLeft {
				dir = left
				x--
			} else if dir == up && out == turnRight {
				dir = right
				x++
			} else if dir == right && out == turnLeft {
				dir = up
				y++
			} else if dir == right && out == turnRight {
				dir = down
				y--
			} else if dir == down && out == turnLeft {
				dir = right
				x++
			} else if dir == down && out == turnRight {
				dir = left
				x--
			} else if dir == left && out == turnLeft {
				dir = down
				y--
			} else if dir == left && out == turnRight {
				dir = up
				y++
			}

			if x <= r.boundLeft {
				r.boundLeft = x - 1
			} else if x >= r.boundRight {
				r.boundRight = x + 1
			} else if y <= r.boundBottom {
				r.boundBottom = y - 1
			} else if y >= r.boundTop {
				r.boundTop = y + 1
			}

			r.cpu.Inputs = []int{r.panels[coordinate{X: x, Y: y}]}
		}

		outputCount++
	}
	r.cpu.Run()
}

func (r *paintRobot) Print() {
	for y := r.boundTop; y >= r.boundBottom; y-- {
		line := ""
		for x := r.boundLeft; x <= r.boundRight; x++ {
			color := r.panels[coordinate{X: x, Y: y}]
			if color == black {
				line += "."
			} else if color == white {
				line += "#"
			}
		}
		fmt.Println(line)
	}
}

func (r *paintRobot) Reset() {
	r.panels = map[coordinate]int{}
	r.paintedPanels = map[coordinate]bool{}
	r.boundBottom = 0
	r.boundLeft = 0
	r.boundRight = 0
	r.boundTop = 0
	r.cpu.Reset()
}
