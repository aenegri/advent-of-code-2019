package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// RunDay14 solves for day 14.
func RunDay14() {
	nano := NanofactoryFromFile("./input/day14part1.txt")

	fmt.Println("Part 1 - Fuel Production")
	fmt.Printf("===> Output: %d\n", nano.OrePerFuel())
	fmt.Println("Part 2 - Total Fuel Produced")
	fmt.Printf("===> Output: %d\n", nano.FuelFromOre(1000000000000))
}

// ReactionOperand is the quantity of a specific
// chemical which is involved in a reaction.
type ReactionOperand struct {
	Chemical string
	Quantity int
}

// Reaction is a formula describing a chemical reaction.
type Reaction struct {
	Input  []ReactionOperand
	Output ReactionOperand
}

// Nanofactory converts raw materials (ORE) into fuel.
type Nanofactory struct {
	Inventory map[string]int
	Reactions map[string]Reaction
}

// NewNanofactory creates a new nanofactory which uses the reaction
// formulas provided.
func NewNanofactory(reactions []Reaction) *Nanofactory {
	chemicalToReaction := map[string]Reaction{}
	for _, r := range reactions {
		chemicalToReaction[r.Output.Chemical] = r
	}
	return &Nanofactory{
		Inventory: map[string]int{},
		Reactions: chemicalToReaction,
	}
}

// NanofactoryFromFile creates a new nanofactory which uses the reaction
// formulas parsed from the provided file.
func NanofactoryFromFile(path string) *Nanofactory {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(data), "\n")
	reactions := []Reaction{}

	for _, line := range lines {
		sides := strings.Split(line, "=>")
		left := sides[0]
		right := sides[1]
		inputs := strings.Split(left, ",")

		regex := regexp.MustCompile(`(\d+)\s*([A-Z]*)`)
		reactionInputs := []ReactionOperand{}
		for _, input := range inputs {
			parts := regex.FindStringSubmatch(input)
			quantity, err := strconv.Atoi(parts[1])
			if err != nil {
				log.Fatal(err)
			}
			reactionInputs = append(
				reactionInputs,
				ReactionOperand{
					Chemical: parts[2],
					Quantity: quantity,
				},
			)
		}
		parts := regex.FindStringSubmatch(right)
		quantity, err := strconv.Atoi(parts[1])
		if err != nil {
			log.Fatal(err)
		}

		reactions = append(reactions, Reaction{
			Input:  reactionInputs,
			Output: ReactionOperand{Chemical: parts[2], Quantity: quantity},
		})
	}

	return NewNanofactory(reactions)
}

// OrePerFuel calculates the minimum quantity of ore needed
// to produce one unit of fuel.
func (nano *Nanofactory) OrePerFuel() int {
	productionQueue := []ReactionOperand{ReactionOperand{Chemical: "FUEL", Quantity: 1}}
	totalOre := 0

	for len(productionQueue) > 0 {
		neededMaterial := productionQueue[0]
		productionQueue = productionQueue[1:]

		// handle base case
		if neededMaterial.Chemical == "ORE" {
			totalOre += neededMaterial.Quantity
			continue
		}

		// get materials from inventory if possible
		quantityAvailable := nano.Inventory[neededMaterial.Chemical]
		if quantityAvailable > neededMaterial.Quantity {
			nano.Inventory[neededMaterial.Chemical] = quantityAvailable - neededMaterial.Quantity
			neededMaterial.Quantity = 0
		} else {
			nano.Inventory[neededMaterial.Chemical] = 0
			neededMaterial.Quantity -= quantityAvailable
		}

		// produce new materials via reaction
		if neededMaterial.Quantity > 0 {
			reaction := nano.Reactions[neededMaterial.Chemical]

			// calculate number of times recipe needs to run
			scale := neededMaterial.Quantity / reaction.Output.Quantity
			if neededMaterial.Quantity%reaction.Output.Quantity > 0 {
				scale++
			}

			// put extra chemicals into inventory
			quantityProduced := scale * reaction.Output.Quantity
			quantityLeftover := quantityProduced - neededMaterial.Quantity

			nano.Inventory[neededMaterial.Chemical] = quantityLeftover

			// iterate over inputs and add to production queue, multiplied by number of reactions needed
			for _, operand := range reaction.Input {
				operand.Quantity *= scale
				productionQueue = append(productionQueue, operand)
			}
		}
	}

	return totalOre
}

// FuelFromOre gets the most fuel that can be produced from the specified
// amount of ore.
func (nano *Nanofactory) FuelFromOre(ore int) int {
	nano.Inventory = map[string]int{}
	// iterate through process of fuel to ore to use up fuel
	// maintain inventory between each loop
	// stop loop when ore reaches 0

	// to optimize: leftover materials should be same for each iteration,
	// so simply multiply the leftovers from one fuel production by
	// (ore / orePerFuel) and see how much fuel can be created with
	// remaining materials

	fuel := 0
	for ore > 0 {
		productionQueue := []ReactionOperand{ReactionOperand{Chemical: "FUEL", Quantity: 1}}

		for len(productionQueue) > 0 && ore > 0 {
			neededMaterial := productionQueue[0]
			productionQueue = productionQueue[1:]

			// handle base case
			if neededMaterial.Chemical == "ORE" {
				ore -= neededMaterial.Quantity
				continue
			}

			// get materials from inventory if possible
			quantityAvailable := nano.Inventory[neededMaterial.Chemical]
			if quantityAvailable > neededMaterial.Quantity {
				nano.Inventory[neededMaterial.Chemical] = quantityAvailable - neededMaterial.Quantity
				neededMaterial.Quantity = 0
			} else {
				nano.Inventory[neededMaterial.Chemical] = 0
				neededMaterial.Quantity -= quantityAvailable
			}

			// produce new materials via reaction
			if neededMaterial.Quantity > 0 {
				reaction := nano.Reactions[neededMaterial.Chemical]

				// calculate number of times recipe needs to run
				scale := neededMaterial.Quantity / reaction.Output.Quantity
				if neededMaterial.Quantity%reaction.Output.Quantity > 0 {
					scale++
				}

				// put extra chemicals into inventory
				quantityProduced := scale * reaction.Output.Quantity
				quantityLeftover := quantityProduced - neededMaterial.Quantity

				nano.Inventory[neededMaterial.Chemical] = quantityLeftover

				// iterate over inputs and add to production queue, multiplied by number of reactions needed
				for _, operand := range reaction.Input {
					operand.Quantity *= scale
					productionQueue = append(productionQueue, operand)
				}
			}
		}

		if ore >= 0 {
			fuel++
		}
	}
	return fuel
}
