package solutions

import (
	"fmt"
	"strconv"
)

// RunDay4 solves for day 4.
func RunDay4() {
	low, hi := 172930, 683082
	fmt.Println("Part 1 - Number of Passwords")
	fmt.Printf("===> Output: %d\n", passwordCount(low, hi))
	fmt.Println("Part 2 - Number of Passwords (Stricter)")
	fmt.Printf("===> Output: %d\n", passwordCountRestricted(low, hi))
}

func passwordCount(low, hi int) int {
	count := 0
	for idx := low; idx <= hi; idx++ {
		chars := strconv.Itoa(idx)

		last := 0
		double, decreasing := false, false
		for _, char := range chars {
			digit := int(char)

			if digit < last {
				decreasing = true
				break
			} else if digit == last {
				double = true
			}

			last = digit
		}

		if double && !decreasing {
			count++
		}
	}

	return count
}

func passwordCountRestricted(low, hi int) int {
	count := 0
	for idx := low; idx <= hi; idx++ {
		chars := strconv.Itoa(idx)

		last, repeatCount := 0, 0
		double, decreasing := false, false
		for i, char := range chars {
			digit := int(char - '0')

			if digit < last {
				repeatCount = 0
				decreasing = true
				break
			} else if digit == last {
				repeatCount++
				if i == len(chars)-1 && repeatCount == 1 {
					double = true
				}
			} else {
				if repeatCount == 1 {
					double = true
				}
				repeatCount = 0
			}
			last = digit
		}

		if double && !decreasing {
			count++
		}
	}

	return count
}
