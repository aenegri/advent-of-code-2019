package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"gitlab.com/aenegri/advent-of-code-2019/intcode"
)

// RunDay9 solves for day 9.
func RunDay9() {
	data, err := ioutil.ReadFile("./input/day9part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	tokens := strings.Split(string(data), ",")

	nums := []int{}
	for _, token := range tokens {
		num, err := strconv.Atoi(token)

		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, num)
	}

	//nums = []int{109, 1, 203, 2, 204, 2, 99}
	intcode := intcode.NewIntcode(intcode.Options{
		Inputs:  []int{1},
		Program: nums,
	})

	intcode.Run()

	fmt.Println("Part 1 - BOOST")
	fmt.Printf("===> Output: %d\n", intcode.Output)

	intcode.Reset()
	intcode.Inputs = []int{2}
	intcode.Run()

	fmt.Println("Part 2 - Coordinates of Distress Signal")
	fmt.Printf("===> Output: %d\n", intcode.Output)
}
