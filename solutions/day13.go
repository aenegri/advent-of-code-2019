package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"gitlab.com/aenegri/advent-of-code-2019/intcode"
)

// RunDay13 solves for day 13.
func RunDay13() {
	data, err := ioutil.ReadFile("./input/day13part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	tokens := strings.Split(string(data), ",")

	nums := []int{}
	for _, token := range tokens {
		num, err := strconv.Atoi(token)

		if err != nil {
			log.Fatal(err)
		}

		nums = append(nums, num)
	}

	arcade := NewArcadeCabinet(nums)
	arcade.Run()
	fmt.Println("Part 1 - Game Start")
	fmt.Printf("===> Output: %d\n", arcade.CountTiles(TileBlock))
	fmt.Println("Part 2 - Play the Game")
	arcade.Reset()
	arcade.EnableFreePlay()
	arcade.Run()
	fmt.Printf("===> Output: %d\n", arcade.Score())
}

// Tiles types used in the arcade game.
const (
	TileEmpty  = 0
	TileWall   = 1
	TileBlock  = 2
	TilePaddle = 3
	TileBall   = 4

	StickLeft    = -1
	StickNeutral = 0
	StickRight   = 1
)

// ArcadeCabinet runs games with the intcode processor.
type ArcadeCabinet struct {
	ballPos   coordinate
	cpu       *intcode.Intcode
	grid      map[coordinate]int
	paddlePos coordinate
	score     int
	xBound    int
	yBound    int
}

// NewArcadeCabinet creates a new arcade cabinet with the
// game loaded.
func NewArcadeCabinet(game []int) *ArcadeCabinet {
	intcodeOptions := intcode.Options{
		Program: game,
	}
	return &ArcadeCabinet{
		cpu:    intcode.NewIntcode(intcodeOptions),
		grid:   map[coordinate]int{},
		xBound: 0,
		yBound: 0,
	}
}

// Run runs the game until it terminates.
func (arcade *ArcadeCabinet) Run() {
	outputCount := 0
	x, y, tileID := 0, 0, 0
	arcade.cpu.OnOutput = func(out int) {
		switch outputCount {
		case 0:
			x = out
			if x >= arcade.xBound {
				arcade.xBound = x + 1
			}
		case 1:
			y = out
			if y >= arcade.yBound {
				arcade.yBound = y + 1
			}
		case 2:
			if x == -1 && y == 0 {
				arcade.score = out
			} else {
				tileID = out
				coord := coordinate{X: x, Y: y}
				arcade.grid[coord] = tileID

				if tileID == TileBall {
					arcade.ballPos = coord
				} else if tileID == TilePaddle {
					arcade.paddlePos = coord
				}
			}
		}
		outputCount++
		outputCount = outputCount % 3
	}
	arcade.cpu.OnInput = func() int {
		// arcade.Print()

		if arcade.ballPos.X > arcade.paddlePos.X {
			return StickRight
		} else if arcade.ballPos.X < arcade.paddlePos.X {
			return StickLeft
		}

		return StickNeutral
	}
	arcade.cpu.Run()
}

// Print writes the current game state to standard out.
func (arcade *ArcadeCabinet) Print() {
	for y := 0; y < arcade.yBound; y++ {
		line := ""
		for x := 0; x < arcade.xBound; x++ {
			tileID := arcade.grid[coordinate{X: x, Y: y}]
			if tileID == TileEmpty {
				line += " "
			} else if tileID == TileWall {
				line += "|"
			} else if tileID == TileBlock {
				line += "#"
			} else if tileID == TilePaddle {
				line += "^"
			} else if tileID == TileBall {
				line += "@"
			}
		}
		fmt.Println(line)
	}
	fmt.Printf("Score: %d\n", arcade.score)
}

// CountTiles returns the total number of tiles
// which match the target tile id.
func (arcade *ArcadeCabinet) CountTiles(targetTile int) int {
	count := 0
	for _, tileID := range arcade.grid {
		if tileID == targetTile {
			count++
		}
	}
	return count
}

// EnableFreePlay allows the game to run without quarters.
func (arcade *ArcadeCabinet) EnableFreePlay() {
	arcade.cpu.Program[0] = 2
}

// Reset returns the arcade game to the original state.
func (arcade *ArcadeCabinet) Reset() {
	arcade.cpu.Reset()
	arcade.grid = map[coordinate]int{}
	arcade.xBound = 0
	arcade.yBound = 0
	arcade.score = 0
}

// Score returns the current arcade game score.
func (arcade *ArcadeCabinet) Score() int {
	return arcade.score
}
