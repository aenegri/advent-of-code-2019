package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"sort"
	"strings"
)

// RunDay10 solves for day 10.
func RunDay10() {
	data, err := ioutil.ReadFile("./input/day10part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	rows := strings.Split(string(data), "\n")
	fmt.Println("Part 1 - Monitoring Station")
	fmt.Printf("===> Output: %d\n", maxAsteroidsVisible(rows))
	fmt.Println("Part 2 - Vaporization Order")
	fmt.Printf("===> Output: %d\n", vaporizeOrder(rows, 200))
}

func maxAsteroidsVisible(rows []string) int {
	linesOfSight := getLinesOfSight(rows)
	bestStation := getBestMonitoringStation(linesOfSight)
	return len(linesOfSight[bestStation])
}

func getBestMonitoringStation(linesOfSight map[coordinate]map[int][]coordinate) coordinate {
	max := 0
	bestStation := coordinate{}
	for coord, angles := range linesOfSight {
		if len(angles) > max {
			max = len(angles)
			bestStation = coord
		}
	}
	return bestStation
}

func getLinesOfSight(rows []string) map[coordinate]map[int][]coordinate {
	linesOfSight := map[coordinate]map[int][]coordinate{}
	precisionFactor := 1000000
	for y, row := range rows {
		for x, r := range row {
			if r == '.' {
				// empty space
				continue
			} else if r != '#' {
				log.Fatalf("Invalid rune '%v' at (%d, %d)", r, x, y)
			}

			pos := coordinate{X: x, Y: y}
			anglesFromCurrent := map[int][]coordinate{}
			for coord, angles := range linesOfSight {
				angle := int(((math.Atan2(float64(y-coord.Y), float64(x-coord.X))*180/math.Pi)+90)*float64(precisionFactor)) % (360 * precisionFactor)
				inverseAngle := (angle + (180 * precisionFactor)) % (360 * precisionFactor)
				angles[angle] = append(angles[angle], pos)
				anglesFromCurrent[inverseAngle] = append(anglesFromCurrent[inverseAngle], coord)
				//fmt.Printf("base: %v, coord: %v, angleCurrent: %d, reverse: %d\n", pos, coord, inverseAngle, angle)
			}

			linesOfSight[coordinate{X: x, Y: y}] = anglesFromCurrent
		}
	}

	return linesOfSight
}

func vaporizeOrder(rows []string, target int) int {
	linesOfSight := getLinesOfSight(rows)
	bestStation := getBestMonitoringStation(linesOfSight)
	anglesToAsteroids := linesOfSight[bestStation]
	keys := make([]int, len(anglesToAsteroids))
	index := 0

	for angle := range anglesToAsteroids {
		keys[index] = angle
		index++
	}

	sort.Ints(keys)

	index = 0
	vaporizeCount := 0
	for vaporizeCount < target-1 {
		angle := keys[index]
		asteroidsInPath := anglesToAsteroids[angle]

		if asteroidsInPath == nil {
			// all asteroids at current angle are vaporized
			index++
			index = index % len(keys)
			continue
		}

		// find closest asteroid
		shortestDistance := 1000000
		closestAsteroidIndex := 0
		for i, asteroid := range asteroidsInPath {
			dist := distance(asteroid, bestStation)

			if dist < shortestDistance {
				shortestDistance = dist
				closestAsteroidIndex = i
			}
		}

		// remove closest asteroid at current angle
		if len(asteroidsInPath) > 1 {
			anglesToAsteroids[angle] = append(asteroidsInPath[0:closestAsteroidIndex], asteroidsInPath[closestAsteroidIndex+1:len(asteroidsInPath)]...)
		} else {
			anglesToAsteroids[angle] = nil
		}

		index++
		index = index % len(keys)
		vaporizeCount++
	}

	for anglesToAsteroids[keys[index]] == nil {
		index++
	}

	asteroidsInPath := anglesToAsteroids[keys[index]]
	shortestDistance := 1000000
	closestAsteroidIndex := 0
	for i, asteroid := range asteroidsInPath {
		dist := distance(asteroid, bestStation)

		if dist < shortestDistance {
			shortestDistance = dist
			closestAsteroidIndex = i
		}
	}
	targetAsteroid := asteroidsInPath[closestAsteroidIndex]

	return targetAsteroid.X*100 + targetAsteroid.Y
}
