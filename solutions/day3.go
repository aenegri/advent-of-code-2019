package solutions

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

// RunDay3 solves for day 3.
func RunDay3() {
	data, err := ioutil.ReadFile("input/day3part1.txt")

	if err != nil {
		log.Fatal(err)
	}

	parts := strings.Split(string(data), "\n")
	wireA := parts[0]
	wireB := parts[1]

	pathA := parseVectors(wireA)
	pathB := parseVectors(wireB)

	closest := findClosestIntersection(pathA, pathB)
	fmt.Println("Part 1 - Crossing the Wires")
	fmt.Printf("===> Output: %d\n", closest)
	fmt.Println("Part 2 - Crossing the Wires Again")
	fmt.Printf("===> Output: %d\n", findShortestSignalDelay(pathA, pathB))
}

func parseVectors(inputs string) []vector {
	rawVectors := strings.Split(inputs, ",")

	vectors := []vector{}
	for _, rawVector := range rawVectors {
		dir := rawVector[0:1]
		dist, err := strconv.Atoi(rawVector[1:])

		if err != nil {
			log.Fatal(err)
		}

		switch dir {
		case "U":
			vectors = append(vectors, vector{direction: up, distance: dist})
		case "R":
			vectors = append(vectors, vector{direction: right, distance: dist})
		case "D":
			vectors = append(vectors, vector{direction: down, distance: dist})
		case "L":
			vectors = append(vectors, vector{direction: left, distance: dist})
		}
	}

	return vectors
}

const (
	up = iota
	right
	down
	left
)

type vector struct {
	direction int
	distance  int
}

type coordinate struct {
	X, Y int
}

func distance(a, b coordinate) int {
	deltaX := a.X - b.X
	deltaY := a.Y - b.Y
	if deltaX < 0 {
		deltaX = -deltaX
	}
	if deltaY < 0 {
		deltaY = -deltaY
	}
	return deltaX + deltaY
}

func findClosestIntersection(a []vector, b []vector) int {
	visited := make(map[coordinate]bool)

	x, y := 0, 0
	for _, vector := range a {
		switch vector.direction {
		case up:
			for idx := y + 1; idx <= y+vector.distance; idx++ {
				visited[coordinate{X: x, Y: idx}] = true
			}
			y += vector.distance
		case right:
			for idx := x + 1; idx <= x+vector.distance; idx++ {
				visited[coordinate{X: idx, Y: y}] = true
			}
			x += vector.distance
		case down:
			for idx := y - 1; idx >= y-vector.distance; idx-- {
				visited[coordinate{X: x, Y: idx}] = true
			}
			y -= vector.distance
		case left:
			for idx := x - 1; idx >= x-vector.distance; idx-- {
				visited[coordinate{X: idx, Y: y}] = true
			}
			x -= vector.distance
		}
	}

	closestIntersection := math.MaxInt64 // todo - init to something
	x, y = 0, 0
	for _, vector := range b {
		switch vector.direction {
		case up:
			for idx := y + 1; idx <= y+vector.distance; idx++ {
				if visited[coordinate{X: x, Y: idx}] {
					dist := abs(x) + abs(idx)
					closestIntersection = min(dist, closestIntersection)
				}
			}
			y += vector.distance
		case right:
			for idx := x + 1; idx <= x+vector.distance; idx++ {
				if visited[coordinate{X: idx, Y: y}] {
					dist := abs(y) + abs(idx)
					closestIntersection = min(dist, closestIntersection)
				}
			}
			x += vector.distance
		case down:
			for idx := y - 1; idx >= y-vector.distance; idx-- {
				if visited[coordinate{X: x, Y: idx}] {
					dist := abs(x) + abs(idx)
					closestIntersection = min(dist, closestIntersection)
				}
			}
			y -= vector.distance
		case left:
			for idx := x - 1; idx >= x-vector.distance; idx-- {
				if visited[coordinate{X: idx, Y: y}] {
					dist := abs(y) + abs(idx)
					closestIntersection = min(dist, closestIntersection)
				}
			}
			x -= vector.distance
		}
	}

	return closestIntersection
}

func findShortestSignalDelay(a []vector, b []vector) int {
	wireDistance := make(map[coordinate]int)

	x, y, distSum := 0, 0, 0
	for _, vector := range a {
		switch vector.direction {
		case up:
			for idx := y + 1; idx <= y+vector.distance; idx++ {
				distSum++
				if wireDistance[coordinate{X: x, Y: idx}] == 0 {
					wireDistance[coordinate{X: x, Y: idx}] = distSum
				}
			}
			y += vector.distance
		case right:
			for idx := x + 1; idx <= x+vector.distance; idx++ {
				distSum++
				if wireDistance[coordinate{X: idx, Y: y}] == 0 {
					wireDistance[coordinate{X: idx, Y: y}] = distSum
				}
			}
			x += vector.distance
		case down:
			for idx := y - 1; idx >= y-vector.distance; idx-- {
				distSum++
				if wireDistance[coordinate{X: x, Y: idx}] == 0 {
					wireDistance[coordinate{X: x, Y: idx}] = distSum
				}
			}
			y -= vector.distance
		case left:
			for idx := x - 1; idx >= x-vector.distance; idx-- {
				distSum++
				if wireDistance[coordinate{X: idx, Y: y}] == 0 {
					wireDistance[coordinate{X: idx, Y: y}] = distSum
				}
			}
			x -= vector.distance
		}
	}

	closestIntersection := math.MaxInt64 // todo - init to something
	x, y, distSum = 0, 0, 0
	for _, vector := range b {
		switch vector.direction {
		case up:
			for idx := y + 1; idx <= y+vector.distance; idx++ {
				distSum++
				if wireDistance[coordinate{X: x, Y: idx}] > 0 {
					dist := wireDistance[coordinate{X: x, Y: idx}] + distSum
					closestIntersection = min(dist, closestIntersection)
				}
			}
			y += vector.distance
		case right:
			for idx := x + 1; idx <= x+vector.distance; idx++ {
				distSum++
				if wireDistance[coordinate{X: idx, Y: y}] > 0 {
					dist := wireDistance[coordinate{X: idx, Y: y}] + distSum
					closestIntersection = min(dist, closestIntersection)
				}
			}
			x += vector.distance
		case down:
			for idx := y - 1; idx >= y-vector.distance; idx-- {
				distSum++
				if wireDistance[coordinate{X: x, Y: idx}] > 0 {
					dist := wireDistance[coordinate{X: x, Y: idx}] + distSum
					closestIntersection = min(dist, closestIntersection)
				}
			}
			y -= vector.distance
		case left:
			for idx := x - 1; idx >= x-vector.distance; idx-- {
				distSum++
				if wireDistance[coordinate{X: idx, Y: y}] > 0 {
					dist := wireDistance[coordinate{X: idx, Y: y}] + distSum
					closestIntersection = min(dist, closestIntersection)
				}
			}
			x -= vector.distance
		}
	}

	return closestIntersection
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
