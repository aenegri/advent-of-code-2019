package ampcircuit

import (
	"gitlab.com/aenegri/advent-of-code-2019/intcode"
)

// Circuit is a circuit consisting of amplifiers connected
// in series.
type Circuit struct {
	feedback   bool
	processors []*intcode.Intcode
	Program    []int
	Phases     []int
	length     int
}

// NewCircuit creates a new amplification circuit.
func NewCircuit(program []int, phases []int, feedback bool) *Circuit {
	processors := make([]*intcode.Intcode, len(phases))

	for i := 0; i < len(phases); i++ {
		processors[i] = intcode.NewIntcode(intcode.Options{
			Program: program,
		})
	}
	return &Circuit{
		feedback:   feedback,
		length:     len(phases),
		Phases:     phases,
		processors: processors,
	}
}

// GetMaxOutput tries all possible phase permutations
// and returns the highest output.
func (circuit *Circuit) GetMaxOutput() int {
	combos := getPermutations(circuit.Phases)
	var max int
	for i, combo := range combos {
		out := circuit.getOutput(combo)

		if i == 0 || out > max {
			max = out
		}

		if circuit.feedback {
			circuit.Reset()
		}
	}
	return max
}

func (circuit *Circuit) getOutput(input []int) int {
	index, out, loops := 0, 0, 0
	exit := false

	for (!circuit.feedback && index < circuit.length) || (circuit.feedback && !exit) {

		intcode := circuit.processors[index]

		if circuit.feedback {
			intcode.OnOutput = func(output int) {
				intcode.Pause()
				out = intcode.Output
			}
			intcode.OnExit = func() {
				exit = true
			}

			if loops >= 1 {
				intcode.Inputs = []int{out}
			} else {
				intcode.Inputs = []int{input[index], out}
			}

			intcode.Run()
		} else {
			intcode.Inputs = []int{input[index], out}
			intcode.Reset()

			intcode.Run()
			out = intcode.Output
		}

		index++

		if circuit.feedback {
			index = index % circuit.length

			if index == 0 {
				loops++
			}
		}
	}

	return out
}

// Reset returns the amplification circuit to the original state.
func (circuit *Circuit) Reset() {
	for _, processor := range circuit.processors {
		processor.Reset()
	}
}

func onOutputFeedbackMode(output int) {
	// pause current intcode in place
	// set the input of the next intcode to the output
	// continue running the next one
}

func getPermutations(nums []int) [][]int {
	permutations := 1
	for i := 2; i <= len(nums); i++ {
		permutations *= i
	}
	combos := make([][]int, permutations)
	count := 0
	getPermutationsHelper(nums, len(nums), &count, combos)
	return combos
}

func getPermutationsHelper(nums []int, k int, count *int, combos [][]int) {
	if k == 0 {
		combo := make([]int, len(nums))
		copy(combo, nums)
		combos[*count] = combo
		*count++
	} else {
		getPermutationsHelper(nums, k-1, count, combos)

		for i := 0; i < k-1; i++ {
			if i%2 == 0 {
				swap(nums, i, k-1)
			} else {
				swap(nums, 0, k-1)
			}
			getPermutationsHelper(nums, k-1, count, combos)
		}
	}
}

func swap(nums []int, i, j int) {
	temp := nums[i]
	nums[i] = nums[j]
	nums[j] = temp
}
