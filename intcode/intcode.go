package intcode

import (
	"log"
	"strconv"
)

// OnOutput is a lifecycle hook for when the intcode processor
// outputs a value.
type OnOutput func(int)

// OnExit is a lifecycle hook for when the intcode processor
// terminates.
type OnExit func()

// OnInput is a lifecycle hook for when the intcode processor
// requires an input.
type OnInput func() int

// Options configures the execution
// of the intcode processor.
type Options struct {
	Inputs   []int
	OnExit   OnExit
	OnInput  OnInput
	OnOutput OnOutput
	Program  []int
}

// Intcode is a processor used to run intcode programs.
type Intcode struct {
	initialProgram     map[int]int
	Inputs             []int
	InstructionPointer int
	OnExit             OnExit
	OnInput            OnInput
	OnOutput           OnOutput
	Output             int
	paused             bool
	Program            map[int]int
	relativeBase       int
}

// NewIntcode instantiates an intcode processor
func NewIntcode(options Options) *Intcode {
	programInMemory := map[int]int{}
	programInitial := map[int]int{}

	for index, num := range options.Program {
		programInMemory[index] = num
		programInitial[index] = num
	}

	return &Intcode{
		initialProgram:     programInitial,
		Inputs:             options.Inputs,
		InstructionPointer: 0,
		OnExit:             options.OnExit,
		OnInput:            options.OnInput,
		OnOutput:           options.OnOutput,
		Output:             0,
		paused:             false,
		Program:            programInMemory,
		relativeBase:       0,
	}
}

// Pause stops execution of the currently running program.
func (intcode *Intcode) Pause() {
	intcode.paused = true
}

// Reset returns the memory to the initial state.
func (intcode *Intcode) Reset() {
	intcode.Program = intcode.initialProgram
	intcode.InstructionPointer = 0
	intcode.relativeBase = 0
}

// Run continues execution of the program, starting where it left off.
// Execution does not stop until a terminate instruction is encountered,
// or the incode processor is paused.
func (intcode *Intcode) Run() {
	prog := intcode.Program
	intcode.paused = false

	inputUsed := false

	for intcode.InstructionPointer < len(prog) && !intcode.paused {
		instruction := strconv.Itoa(prog[intcode.InstructionPointer])

		op := prog[intcode.InstructionPointer] % 100

		paramModes := []int{}

		for charPos := len(instruction) - 3; charPos >= 0; charPos-- {
			mode, err := strconv.Atoi(instruction[charPos : charPos+1])

			if err != nil {
				log.Fatal(err)
			}

			paramModes = append(paramModes, mode)
		}

		var operands []int
		//fmt.Printf("%d, %d, %d, %d\n", prog[intcode.InstructionPointer], prog[intcode.InstructionPointer+1], prog[intcode.InstructionPointer+2], prog[intcode.InstructionPointer+3])
		switch op {
		case 1:
			// addition
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 3, paramModes)

			prog[operands[2]] = prog[operands[0]] + prog[operands[1]]
			intcode.InstructionPointer += 4
		case 2:
			// multiplication
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 3, paramModes)

			prog[operands[2]] = prog[operands[0]] * prog[operands[1]]
			intcode.InstructionPointer += 4
		case 3:
			// input
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 1, paramModes)
			input := 0
			if intcode.OnInput != nil {
				input = intcode.OnInput()
			} else {
				inputs := intcode.Inputs
				if inputUsed && len(inputs) > 1 {
					input = inputs[1]
				} else {
					input = inputs[0]
					inputUsed = true
				}
			}

			prog[operands[0]] = input

			intcode.InstructionPointer += 2
		case 4:
			// output
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 1, paramModes)

			intcode.Output = prog[operands[0]]

			intcode.InstructionPointer += 2

			if intcode.OnOutput != nil {
				intcode.OnOutput(prog[operands[0]])
			}
		case 5:
			// jump if true
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 2, paramModes)

			if prog[operands[0]] != 0 {
				intcode.InstructionPointer = prog[operands[1]]
			} else {
				intcode.InstructionPointer += 3
			}
		case 6:
			// jump if false
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 2, paramModes)

			if prog[operands[0]] == 0 {
				intcode.InstructionPointer = prog[operands[1]]
			} else {
				intcode.InstructionPointer += 3
			}
		case 7:
			// less than
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 3, paramModes)

			if prog[operands[0]] < prog[operands[1]] {
				prog[operands[2]] = 1
			} else {
				prog[operands[2]] = 0
			}

			intcode.InstructionPointer += 4
		case 8:
			// equals
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 3, paramModes)

			if prog[operands[0]] == prog[operands[1]] {
				prog[operands[2]] = 1
			} else {
				prog[operands[2]] = 0
			}

			intcode.InstructionPointer += 4
		case 9:
			// change relative base
			operands = intcode.getIntcodeOperands(op, intcode.InstructionPointer+1, 1, paramModes)

			intcode.relativeBase += prog[operands[0]]

			intcode.InstructionPointer += 2
		case 99:
			intcode.paused = true
			if intcode.OnExit != nil {
				intcode.OnExit()
			}
		default:
			log.Fatalf("Invalid opcode: %d", op)
		}
		// fmt.Printf("Instruction: %s, Modes: %v, Op: %d, Operands: %v, Next: %d\n", instruction, paramModes, op, operands, prog[intcode.InstructionPointer+1])
	}
}

// getIntcodeOperands retrieves the operands for the next n indices based
// on the configured modes.
func (intcode *Intcode) getIntcodeOperands(op int, start int, n int, modes []int) []int {
	operands := []int{}
	program := intcode.Program

	// todo - fix mode handling when writing (203 error)
	// https://www.reddit.com/r/adventofcode/comments/e8aw9j/2019_day_9_part_1_how_to_fix_203_error/
	for index := 0; index < n; index++ {
		// if parameter is used for writing, the mode cannot be immediate mode
		writeParameter := ((op == 1 || op == 2 || op == 7 || op == 8) && index == 2) || (op == 3 && index == 0)

		if index >= len(modes) || modes[index] == 0 || (modes[index] == 1 && writeParameter) {
			// position mode
			operands = append(operands, program[start+index])
		} else if modes[index] == 1 && !writeParameter {
			// immediate mode
			operands = append(operands, start+index)
		} else {
			// relative mode
			operands = append(operands, program[start+index]+intcode.relativeBase)
		}
	}
	return operands
}
