package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/aenegri/advent-of-code-2019/solutions"
)

func main() {
	var day int

	flag.IntVar(&day, "day", 0, "Select which day to solve")

	flag.Parse()

	switch day {
	case 0:
		fmt.Println("Please specify a day to solve")
	case 1:
		solutions.RunDay1()
	case 2:
		solutions.RunDay2()
	case 3:
		solutions.RunDay3()
	case 4:
		solutions.RunDay4()
	case 5:
		solutions.RunDay5()
	case 6:
		solutions.RunDay6()
	case 7:
		solutions.RunDay7()
	case 8:
		solutions.RunDay8()
	case 9:
		solutions.RunDay9()
	case 10:
		solutions.RunDay10()
	case 11:
		solutions.RunDay11()
	case 12:
		solutions.RunDay12()
	case 13:
		solutions.RunDay13()
	case 14:
		solutions.RunDay14()
	default:
		log.Fatalf("Day %d not implemented", day)
	}
}
